Lenguajes y Paradigmas de Programación
==================

Práctica 6 - Desarrollo Dirigido por Pruebas (TDD)
-----------

Autor:

* Mauricio Orta

Descripción
----------------------

Este repositorio contiene un árbol de directorios y ficheros creados con la herramienta Bundler, la cual organiza la estructura necesaria para 
poder obtener una "gema" o librería de ruby.

Se incluye un fichero biblioref.rb con una clase Biblioref para el manejo de referencias bibliográficas de libros. Igualmente existe un
fichero biblioref_spec.rb en la cual, siguiendo el esquema de Desarrollo Dirigido por Pruebas, se escribieron ejemplos para ir probando
el código de la clase a medida que se construía.

Árbol de ficheros y directorios
-------------------------------
``` 
.
├── CODE_OF_CONDUCT.md
├── Gemfile
├── LICENSE.txt
├── README.md
├── Rakefile
├── biblioref.gemspec
├── bin
│   ├── console
│   └── setup
├── lib
│   ├── biblioref
│   │   └── version.rb
│   └── biblioref.rb
└── spec
    ├── biblioref_spec.rb
    └── spec_helper.rb
``` 
    
---------------------------
