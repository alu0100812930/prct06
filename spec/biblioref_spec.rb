require 'spec_helper'

describe Biblioref do
  before:each do
    @libro = Biblioref.new("Dave Thomas", "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide", "Pragmatic Bookshelf", 4, "July 7, 2013", "ISBN-13: 978-1937785499")
     @libro2 = Biblioref.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide", "Pragmatic Bookshelf", 4, "July 7, 2013", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"], "(The Facets of Ruby)")
end

describe "Nuevo libro" do
    it "Toma los cinco parámetros obligatorios o también el opcional de serie y retorna un objeto de clase Biblioref" do
        expect(@libro).to be_a Biblioref
        expect(@libro2).to be_a Biblioref
    end
end

describe "Autores" do
    it "Puede haber uno o más autores en la referencia" do
      expect(@libro.autors).to be == ["Dave Thomas"]
      expect(@libro.autors.count).to be == 1
      expect(@libro2.autors).to be == ["Dave Thomas", "Andy Hunt", "Chad Fowler"]
      expect(@libro2.autors.count).to be > 1
    end
end

describe "Título" do
    it "Debe haber un título" do
      expect(@libro.title).to be == "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide"
      expect(@libro2.title).to be == "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide"
    end
end

describe "Serie" do
    it "Puede haber o no una serie" do
      expect(@libro.series).to be == ""
      expect(@libro2.series).to be == "(The Facets of Ruby)"
    end
end

describe "Editorial" do
    it "Debe existir una editorial" do
      expect(@libro.p_house).to be == "Pragmatic Bookshelf"
      expect(@libro2.p_house).to be == "Pragmatic Bookshelf"
    end
end

describe "Nº de edición" do
    it "Debe existir un número de edición" do
      expect(@libro.edit_num).to be == 4
      expect(@libro2.edit_num).to be == 4
    end
end

describe "Fecha de publicación" do
    it "Debe existir una fecha de publicación" do
      expect(@libro.p_date).to be == "July 7, 2013"
      expect(@libro2.p_date).to be == "July 7, 2013"
    end
end

describe "Números ISBN" do
    it "Puede haber uno o más números ISBN" do
      expect(@libro.isbn_num).to be== ["ISBN-13: 978-1937785499"]
      expect(@libro.isbn_num.count).to be == 1
      expect(@libro2.isbn_num).to be == ["ISBN-13: 978-1937785499","ISBN-10: 1937785491"]
      expect(@libro2.isbn_num.count).to be > 1
    end
end

describe "Salida formateada" do
    it "La clase debe tener una salida de referencia formateada" do
       expect(@libro.to_s).to be == "Dave Thomas.\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide\nPragmatic Bookshelf; 4 edition (July 7, 2013)\nISBN-13: 978-1937785499\n"
       expect(@libro2.to_s).to be == "Dave Thomas, Andy Hunt, Chad Fowler.\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide\n(The Facets of Ruby)\nPragmatic Bookshelf; 4 edition (July 7, 2013)\nISBN-13: 978-1937785499\nISBN-10: 1937785491\n"
    end
end


end


