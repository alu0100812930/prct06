#!/usr/bin/ruby

require "biblioref/version"

class Biblioref
  attr_accessor :autors
      attr_accessor :title
      attr_accessor :series
      attr_accessor :p_house #Casa editorial
      attr_accessor :edit_num #Numero de edición
      attr_accessor :p_date #Fecha de publicación
      attr_accessor :isbn_num #Números isbn
      def initialize(autor, title, p_house, edit_number, p_date, isbn_num, series="")
          @autors = []
          if autor.class.to_s=="Array"
            for i in 0..autor.count-1
            @autors << autor[i]
          end
          else
            @autors << autor
          end
          @title = title
          @series= series
          @p_house = p_house
          @edit_num = edit_number
          @p_date = p_date
          @isbn_num = []
          if isbn_num.class.to_s=="Array"
            for i in 0..isbn_num.count-1
            @isbn_num << isbn_num[i]
          end
        else
          @isbn_num << isbn_num
          end
          end
      def to_s
          names =""
          isbns =""
          i=0
  while i < @autors.count
if i != @autors.count-1
  names= names + "#{@autors[i]}, "
else
  names= names + "#{@autors[i]}."
end
  i=i+1
end
i=0
while i < @isbn_num.count
  isbns= isbns + "#{@isbn_num[i]}\n"
  i=i+1
end
if series != ""
      "#{names}\n#{@title}\n#{@series}\n#{@p_house}; #{@edit_num} edition (#{@p_date})\n#{isbns}"
else
      "#{names}\n#{@title}\n#{@p_house}; #{@edit_num} edition (#{@p_date})\n#{isbns}"
    end
    end 
end

